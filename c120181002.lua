local m=120181002
local cm=_G["c"..m]
cm.name="黑魔术少女"
function cm.initial_effect(c)
	RD.AddCodeList(c,LEGEND_DARK_MAGICIAN)
	--Atk Up
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_UPDATE_ATTACK)
	e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e1:SetRange(LOCATION_MZONE)
	e1:SetValue(cm.atkval)
	c:RegisterEffect(e1)
end
--Atk Up
function cm.filter(c)
	return RD.IsLegendCode(c,LEGEND_DARK_MAGICIAN)
end
function cm.atkval(e,c)
	return Duel.GetMatchingGroupCount(cm.filter,c:GetControler(),LOCATION_GRAVE,LOCATION_GRAVE,nil)*500
end