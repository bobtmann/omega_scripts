local m=120196003
local list={120196006,120196010}
local cm=_G["c"..m]
cm.name="钢铁徽章之赫拉克勒斯明星"
function cm.initial_effect(c)
	RD.AddCodeList(c,list)
	--Fusion Material
	RD.AddFusionProcedure(c,list[1],list[2])
	--Multi-Choose Effect
	RD.CreateMultiChooseEffect(c,nil,cm.cost,aux.Stringid(m,1),cm.target1,cm.operation1,aux.Stringid(m,2),cm.target2,cm.operation2,CATEGORY_TODECK+CATEGORY_GRAVE_ACTION,CATEGORY_TODECK)
end
--Multi-Choose Effect
cm.cost=RD.CostSendGraveToDeckBottom(Card.IsAbleToDeckOrExtraAsCost,1,1)
--To Deck(Grave)
function cm.filter1(c)
	return c:IsType(TYPE_MONSTER) and c:IsAbleToDeck()
end
function cm.target1(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(cm.filter1,tp,0,LOCATION_GRAVE,1,nil) end
	local g=Duel.GetMatchingGroup(cm.filter1,tp,0,LOCATION_GRAVE,nil)
	Duel.SetOperationInfo(0,CATEGORY_TODECK,g,1,0,0)
end
function cm.operation1(e,tp,eg,ep,ev,re,r,rp)
	RD.SelectAndDoAction(HINTMSG_TODECK,aux.NecroValleyFilter(cm.filter1),tp,0,LOCATION_GRAVE,1,3,nil,function(g)
		RD.SendToDeckAndExists(g)
	end)
end
--To Deck(Monstar)
function cm.filter2(c)
	return c:IsFaceup() and c:IsRace(RACE_SPELLCASTER) and c:IsAbleToDeck()
end
function cm.target2(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(cm.filter2,tp,0,LOCATION_MZONE,1,nil) end
	local g=Duel.GetMatchingGroup(cm.filter2,tp,0,LOCATION_MZONE,nil)
	Duel.SetOperationInfo(0,CATEGORY_TODECK,g,1,0,0)
end
function cm.operation2(e,tp,eg,ep,ev,re,r,rp)
	RD.SelectAndDoAction(HINTMSG_TODECK,cm.filter2,tp,0,LOCATION_MZONE,1,2,nil,function(g)
		RD.SendToDeckAndExists(g)
	end)
end