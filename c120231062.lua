local m=120231062
local cm=_G["c"..m]
cm.name="爱天使"
function cm.initial_effect(c)
	--Indes
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
	e1:SetValue(cm.indes)
	c:RegisterEffect(e1)
end
--Indes
function cm.indes(e,c)
	return c:IsLevel(7,8)
end