local m=120238000
local list={120238029}
local cm=_G["c"..m]
cm.name="流星黑龙"
function cm.initial_effect(c)
	RD.AddCodeList(c,LEGEND_RED_EYES_BLACK_DRAGON,list)
	--Fusion Material
	RD.AddFusionProcedure(c,LEGEND_RED_EYES_BLACK_DRAGON,list[1])
end