local m=120222065
local cm=_G["c"..m]
cm.name="助推救援"
function cm.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_DRAW+CATEGORY_TOGRAVE+CATEGORY_POSITION)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_SUMMON_SUCCESS)
	e1:SetProperty(EFFECT_FLAG_DAMAGE_STEP+EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCondition(cm.condition)
	e1:SetTarget(cm.target)
	e1:SetOperation(cm.activate)
	c:RegisterEffect(e1)
	local e2=e1:Clone()
	e2:SetCode(EVENT_SPSUMMON_SUCCESS)
	c:RegisterEffect(e2)
end
--Activate
function cm.confilter(c,tp)
	return c:GetSummonPlayer()==tp and c:IsFaceup()
end
function cm.posfilter(c)
	return c:IsAttackPos() and RD.IsCanChangePosition(c)
end
function cm.condition(e,tp,eg,ep,ev,re,r,rp)
	return eg:IsExists(cm.confilter,1,nil,1-tp)
end
function cm.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsPlayerCanDraw(tp,2) end
	RD.TargetDraw(tp,2)
end
function cm.activate(e,tp,eg,ep,ev,re,r,rp)
	local p,d=Duel.GetChainInfo(0,CHAININFO_TARGET_PLAYER,CHAININFO_TARGET_PARAM)
	if Duel.Draw(p,d,REASON_EFFECT)==d then
		RD.SelectAndDoAction(HINTMSG_TOGRAVE,Card.IsAbleToGrave,p,LOCATION_HAND,0,1,1,nil,function(g)
			Duel.BreakEffect()
			if Duel.SendtoGrave(g,REASON_EFFECT)~=0 then
				RD.CanSelectAndDoAction(aux.Stringid(m,1),HINTMSG_POSCHANGE,cm.posfilter,tp,LOCATION_MZONE,0,1,2,nil,function(sg)
					RD.ChangePosition(sg,POS_FACEUP_DEFENSE)
				end)
			end
		end)
	end
end